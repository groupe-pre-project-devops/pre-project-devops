## Description

A Laravel project where employees can subscribe to different formations.

## Teams members

- Member n°1: FOUASSIER Guilhem
- Member n°2: VILAY Manida

## What has been done

- 4 different stages: install (install dependencies), tests (test different jobs), clean (clean caches) and deploy
- First feature: unit test
- Second facultative feature: blade test
- Third facultative feature: security checker test

## Deployment URLs

- https://pre-project-devops.herokuapp.com/
- https://pre-project-devops-manida.herokuapp.com/
